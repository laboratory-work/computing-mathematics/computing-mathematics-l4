package ru.lanolin.compmath.l4;

import javafx.application.Application;
import ru.lanolin.compmath.l4.ui.FXMain;

public class Main {
	public static void main(String[] args) {
		Application.launch(FXMain.class, args);
	}
}
