package ru.lanolin.compmath.l4.utils;

import au.com.bytecode.opencsv.CSVReader;
import ru.lanolin.compmath.l4.math.XYSeries;

import java.io.*;
import java.math.BigDecimal;
import java.util.List;

public final class ReadFromFile {

	public static XYSeries read(String s) throws IOException {
		return read(new File(s));
	}

	public static XYSeries read(File f) throws IOException {
		FileReader fr = new FileReader(f);
		CSVReader reader = new CSVReader(fr, ',', '"', 1);

		List<String[]> vars = reader.readAll();
		XYSeries series = new XYSeries(vars.size());

		for(int i = 0; i < vars.size(); i++){
			BigDecimal xi = new BigDecimal(vars.get(i)[0].trim());
			BigDecimal yi = new BigDecimal(vars.get(i)[1].trim());
			series.set(i, xi, yi);
		}
		return series;
	}
}
