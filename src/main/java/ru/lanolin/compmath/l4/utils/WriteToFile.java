package ru.lanolin.compmath.l4.utils;

import au.com.bytecode.opencsv.CSVWriter;
import ru.lanolin.compmath.l4.math.Result;
import ru.lanolin.compmath.l4.math.XYSeries;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class WriteToFile {

	private static final String[] header1 = { "Вид функции", "", "Линейная", "Полиномиальная", "Экспоненциальная", "Логарифмическая", "Степенная"};
	private static final String[] header2 = { "X", "Y", "F=ax+b", "F=ax^2+bx+c", "F=a e^{bx}", "F=a ln(x) + b", "F=a x^b" };
	private static final String[] names = { "S", "sigma", "a", "b", "c" };

	public static void writeToFile(String filename, XYSeries series, Result[] results){
		File f = new File(filename);
		//FIXME: create file. check result file extension
		writeToFile(f, series, results);
	}

	/**
	 *
	 * @param f Файл для записи
	 * @param series - Исходные данные
	 * @param results -> 0 - Линейная, 1 - Полиномиальная, 2 - Экспоненциальная, 3 - Логарифмическая, 4 - Степенная
	 */
	public static void writeToFile(File f, XYSeries series, Result[] results){
		List<String[]> lines = new ArrayList<>();
		lines.add(header1);
		lines.add(header2);

		for(int i = 0; i < series.getN(); i++){
			String[] li = new String[7];
			li[0] = String.format("%.5f", series.getXi(i));
			li[1] = String.format("%.5f", series.getYi(i));
			for(int j = 0; j < 5; j++) {
				li[2 + j] = String.format("%.5f", results[j].getF(i));
			}
			lines.add(li);
		}

		lines.add(new String[]{
				"", names[0],
				String.format("%.5f", results[0].getS()),
				String.format("%.5f", results[1].getS()),
				String.format("%.5f", results[2].getS()),
				String.format("%.5f", results[3].getS()),
				String.format("%.5f", results[4].getS()),
		});

		lines.add(new String[]{
				"", names[1],
				String.format("%.5f", results[0].getSigma()),
				String.format("%.5f", results[1].getSigma()),
				String.format("%.5f", results[2].getSigma()),
				String.format("%.5f", results[3].getSigma()),
				String.format("%.5f", results[4].getSigma()),
		});

		lines.add(new String[]{
				"", names[2],
				String.format("%.5f", results[0].getA()),
				String.format("%.5f", results[1].getA()),
				String.format("%.5f", results[2].getA()),
				String.format("%.5f", results[3].getA()),
				String.format("%.5f", results[4].getA()),
		});

		lines.add(new String[]{
				"", names[3],
				String.format("%.5f", results[0].getB()),
				String.format("%.5f", results[1].getB()),
				String.format("%.5f", results[2].getB()),
				String.format("%.5f", results[3].getB()),
				String.format("%.5f", results[4].getB()),
		});

		lines.add(new String[]{
				"", names[4],
				(results[0].getC() == null) ? "-" : String.format("%.5f", results[0].getC()),
				(results[1].getC() == null) ? "-" : String.format("%.5f", results[1].getC()),
				(results[2].getC() == null) ? "-" : String.format("%.5f", results[2].getC()),
				(results[3].getC() == null) ? "-" : String.format("%.5f", results[3].getC()),
				(results[4].getC() == null) ? "-" : String.format("%.5f", results[4].getC()),
		});

		try(FileWriter fr = new FileWriter(f);
			CSVWriter writer = new CSVWriter(fr, ',', '"')){
			writer.writeAll(lines);
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
