package ru.lanolin.compmath.l4.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.lanolin.compmath.l4.Main;
import ru.lanolin.compmath.l4.math.Approximations;
import ru.lanolin.compmath.l4.math.Result;
import ru.lanolin.compmath.l4.math.XYSeries;
import ru.lanolin.compmath.l4.ui.controller.MainController;
import ru.lanolin.compmath.l4.ui.controller.ResultController;

public class FXMain extends Application {

	private Stage mainStage;

	private Scene first;
	private Scene result;

	private MainController mainController;
	private ResultController resultController;

	@Override
	public void init() throws Exception {
		super.init();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.mainStage = primaryStage;

		this.mainStage.setOnCloseRequest(event -> System.exit(0));

		FXMLLoader loader = new FXMLLoader(Main.class.getClassLoader().getResource("views/mainScene.fxml"));
		AnchorPane root =  loader.load();
		mainController = loader.getController();
		mainController.setApplicationController(this);
		this.first = new Scene(root);

		FXMLLoader res = new FXMLLoader(Main.class.getClassLoader().getResource("views/result.fxml"));
		AnchorPane result = res.load();
		resultController = res.getController();
		resultController.setApplication(this);
		this.result = new Scene(result);

		this.mainStage.setScene(first);
		this.mainStage.setResizable(false);
		this.mainStage.setTitle("Апроксимация");
		this.mainStage.show();
	}

	public void solveAction(XYSeries series){
		try {
			Result[] output = new Result[5];
			output[0] = Approximations.Linear.getMethod().solve(series);
			output[0].setFunction(Approximations.Linear);
			output[1] = Approximations.Polinom.getMethod().solve(series);
			output[1].setFunction(Approximations.Polinom);
			output[2] = Approximations.Exponential.getMethod().solve(series);
			output[2].setFunction(Approximations.Exponential);
			output[3] = Approximations.Logarithm.getMethod().solve(series);
			output[3].setFunction(Approximations.Logarithm);
			output[4] = Approximations.Power.getMethod().solve(series);
			output[4].setFunction(Approximations.Power);

			Alert info = new Alert(Alert.AlertType.INFORMATION);
			info.setTitle("Успех");
			info.setHeaderText("Успешно посчитаны апроксимации");
			info.setContentText(null);
			info.setResult(ButtonType.APPLY);
			info.showAndWait();

			resultController.setResults(output, series);
			this.mainStage.setScene(this.result);
			this.mainStage.setTitle("Результат");
			resultController.showResult();
		}catch (Exception e){
			Alert info = new Alert(Alert.AlertType.ERROR);
			info.setTitle("Ошибка");
			info.setHeaderText("Ошибка при расчете");
			info.setContentText(e.getMessage());
			info.setResult(ButtonType.CLOSE);
			info.show();
		}
	}

	public void retry(){
		this.mainStage.setScene(this.first);
		this.mainStage.setTitle("Апроксимация");
	}

	public Stage getMainStage() {
		return mainStage;
	}
}
