package ru.lanolin.compmath.l4.ui;

import javafx.beans.property.SimpleStringProperty;

public class TableResult {

	private SimpleStringProperty first;
	private SimpleStringProperty two;
	private SimpleStringProperty three;
	private SimpleStringProperty four;
	private SimpleStringProperty five;
	private SimpleStringProperty six;
	private SimpleStringProperty seven;

	public TableResult() {
		this.first = new SimpleStringProperty();
		this.two = new SimpleStringProperty();
		this.three = new SimpleStringProperty();
		this.four = new SimpleStringProperty();
		this.five = new SimpleStringProperty();
		this.six = new SimpleStringProperty();
		this.seven = new SimpleStringProperty();
	}

	public TableResult(SimpleStringProperty first, SimpleStringProperty two, SimpleStringProperty three,
					   SimpleStringProperty four, SimpleStringProperty five, SimpleStringProperty six,
					   SimpleStringProperty seven) {
		this.first = first;
		this.two = two;
		this.three = three;
		this.four = four;
		this.five = five;
		this.six = six;
		this.seven = seven;
	}

	public TableResult(String first, String two, String three,
					   String four, String five, String six, String seven){
		this(new SimpleStringProperty(first),
				new SimpleStringProperty(two),
				new SimpleStringProperty(three),
				new SimpleStringProperty(four),
				new SimpleStringProperty(five),
				new SimpleStringProperty(six),
				new SimpleStringProperty(seven));
	}

	public String getFirst() {
		return first.get();
	}

	public SimpleStringProperty firstProperty() {
		return first;
	}

	public void setFirst(String first) {
		this.first.set(first);
	}

	public String getTwo() {
		return two.get();
	}

	public SimpleStringProperty twoProperty() {
		return two;
	}

	public void setTwo(String two) {
		this.two.set(two);
	}

	public String getThree() {
		return three.get();
	}

	public SimpleStringProperty threeProperty() {
		return three;
	}

	public void setThree(String three) {
		this.three.set(three);
	}

	public String getFour() {
		return four.get();
	}

	public SimpleStringProperty fourProperty() {
		return four;
	}

	public void setFour(String four) {
		this.four.set(four);
	}

	public String getFive() {
		return five.get();
	}

	public SimpleStringProperty fiveProperty() {
		return five;
	}

	public void setFive(String five) {
		this.five.set(five);
	}

	public String getSix() {
		return six.get();
	}

	public SimpleStringProperty sixProperty() {
		return six;
	}

	public void setSix(String six) {
		this.six.set(six);
	}

	public String getSeven() {
		return seven.get();
	}

	public SimpleStringProperty sevenProperty() {
		return seven;
	}

	public void setSeven(String seven) {
		this.seven.set(seven);
	}
}
