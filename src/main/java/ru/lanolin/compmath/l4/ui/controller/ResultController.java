package ru.lanolin.compmath.l4.ui.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.lanolin.compmath.l4.math.Result;
import ru.lanolin.compmath.l4.math.XYSeries;
import ru.lanolin.compmath.l4.ui.FXMain;
import ru.lanolin.compmath.l4.ui.LineChart;
import ru.lanolin.compmath.l4.ui.TableResult;
import ru.lanolin.compmath.l4.utils.WriteToFile;

import java.io.File;
import java.io.IOException;

public class ResultController {

	private FXMain application;
	public void setApplication(FXMain application) {
		this.application = application;
	}

	@FXML public TextField bestFunction;
	@FXML public TextField aVar;
	@FXML public TextField bVar;
	@FXML public TextField cVar;
	@FXML public TextField sVar;
	@FXML public TextField sigmaVar;
	@FXML public TableView<TableResult> tables;

	private ObservableList<TableResult> items;

	private Result[] results;
	private XYSeries series;
	private Result bestResult;
	private FileChooser chooser;

	public void setResults(Result[] results, XYSeries series) {
		this.results = results;
		this.series = series;
		bestResult = results[0];
		items.clear();

		for (int i = 0; i < 5; i++) {
			if(bestResult.getS().compareTo(results[i].getS()) > 0){
				bestResult = results[i];
			}
		}

		for (int j = 0; j < series.getN(); j++) {
			TableResult tri = new TableResult(
					String.format("%.5f", series.getXi(j)),
					String.format("%.5f", series.getYi(j)),
					String.format("%.5f", results[0].getF(j)),
					String.format("%.5f", results[1].getF(j)),
					String.format("%.5f", results[2].getF(j)),
					String.format("%.5f", results[3].getF(j)),
					String.format("%.5f", results[4].getF(j))
			);
			items.add(tri);
		}

		TableResult S = new TableResult(
				"",
				"S",
				String.format("%.5f", results[0].getS()),
				String.format("%.5f", results[1].getS()),
				String.format("%.5f", results[2].getS()),
				String.format("%.5f", results[3].getS()),
				String.format("%.5f", results[4].getS())
		);
		TableResult sigma = new TableResult(
				"",
				"sigma",
				String.format("%.5f", results[0].getSigma()),
				String.format("%.5f", results[1].getSigma()),
				String.format("%.5f", results[2].getSigma()),
				String.format("%.5f", results[3].getSigma()),
				String.format("%.5f", results[4].getSigma())
		);
		TableResult a = new TableResult(
				"",
				"a",
				String.format("%.5f", results[0].getA()),
				String.format("%.5f", results[1].getA()),
				String.format("%.5f", results[2].getA()),
				String.format("%.5f", results[3].getA()),
				String.format("%.5f", results[4].getA())
		);
		TableResult b = new TableResult(
				"",
				"b",
				String.format("%.5f", results[0].getB()),
				String.format("%.5f", results[1].getB()),
				String.format("%.5f", results[2].getB()),
				String.format("%.5f", results[3].getB()),
				String.format("%.5f", results[4].getB())
		);
		TableResult c = new TableResult(
				"",
				"c",
				(results[0].getC() == null) ? "-" : String.format("%.5f", results[0].getC()),
				(results[1].getC() == null) ? "-" : String.format("%.5f", results[1].getC()),
				(results[2].getC() == null) ? "-" : String.format("%.5f", results[2].getC()),
				(results[3].getC() == null) ? "-" : String.format("%.5f", results[3].getC()),
				(results[4].getC() == null) ? "-" : String.format("%.5f", results[4].getC())
		);

		items.addAll(S, sigma, a, b, c);
	}

	@FXML
	public void initialize(){
		chooser = new FileChooser();
		FileChooser.ExtensionFilter csv = new FileChooser.ExtensionFilter("CSV (*.csv)", "*.csv");
		chooser.getExtensionFilters().add(csv);

		items = FXCollections.observableArrayList();
		TableColumn<TableResult, String> data = new TableColumn<>("Вид функции");
		TableColumn<TableResult, String> x = new TableColumn<>("X");
		x.setCellValueFactory(new PropertyValueFactory<>("first"));
		TableColumn<TableResult, String> y = new TableColumn<>("Y");
		y.setCellValueFactory(new PropertyValueFactory<>("two"));
		data.getColumns().add(x);
		data.getColumns().add(y);
		tables.getColumns().add(data);
		TableColumn<TableResult, String> linear = new TableColumn<>("Линейная");
		TableColumn<TableResult, String> linearF = new TableColumn<>("F=a*x+b");
		linearF.setCellValueFactory(new PropertyValueFactory<>("three"));
		linear.getColumns().add(linearF);
		tables.getColumns().add(linear);
		TableColumn<TableResult, String> polinom = new TableColumn<>("Полиноминальная");
		TableColumn<TableResult, String> polinomF = new TableColumn<>("F=a*x^2+b*x+c");
		polinomF.setCellValueFactory(new PropertyValueFactory<>("four"));
		polinom.getColumns().add(polinomF);
		tables.getColumns().add(polinom);
		TableColumn<TableResult, String> exp = new TableColumn<>("Экспоненциальная");
		TableColumn<TableResult, String> expF = new TableColumn<>("F=a*e^(b*x)");
		expF.setCellValueFactory(new PropertyValueFactory<>("five"));
		exp.getColumns().add(expF);
		tables.getColumns().add(exp);
		TableColumn<TableResult, String> log = new TableColumn<>("Логарифмическая");
		TableColumn<TableResult, String> logF = new TableColumn<>("F=a*ln(x)+b");
		logF.setCellValueFactory(new PropertyValueFactory<>("six"));
		log.getColumns().add(logF);
		tables.getColumns().add(log);
		TableColumn<TableResult, String> power = new TableColumn<>("Степенная");
		TableColumn<TableResult, String> powerF = new TableColumn<>("F=a*x^b");
		powerF.setCellValueFactory(new PropertyValueFactory<>("seven"));
		power.getColumns().add(powerF);
		tables.getColumns().add(power);

		tables.setItems(items);
	}

	public void showResult(){
		bestFunction.setText(bestResult.getFunction().getName());
		aVar.setText(String.format("%.5f", bestResult.getA()));
		bVar.setText(String.format("%.5f", bestResult.getB()));
		cVar.setText(bestResult.getC() == null ? "-" : String.format("%.5f", bestResult.getC()));
		sVar.setText(String.format("%.5f", bestResult.getS()));
		sigmaVar.setText(String.format("%.5f", bestResult.getSigma()));
	}


	@FXML
	public void repeatAction(ActionEvent actionEvent) {
		application.retry();
	}

	@FXML
	public void showGraphics(ActionEvent actionEvent) {
		for (int i = 0; i < 5; i++) {
			showGraphics(i);
		}
	}

	private void showGraphics(int i){
		SwingNode swingNode = new SwingNode();
		swingNode.setContent(new LineChart(series, results[i]).plot());

		Stage stage = new Stage();
		stage.setScene(new Scene(new StackPane(swingNode)));
		stage.initModality(Modality.NONE);
		stage.setTitle(results[i].getFunction().getName());
		stage.setWidth(300);
		stage.setHeight(300);
		stage.setOnCloseRequest(event -> stage.close());
		stage.show();
	}

	private void showAlert(Alert.AlertType type, String title, String header, String content){
		Alert alert = new Alert(type);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.show();
	}

	@FXML
	public void saveResult(ActionEvent actionEvent) {
		File f = chooser.showSaveDialog(application.getMainStage());
		if(f == null) return;
		if(!f.getName().contains(".csv")){
			f = new File(f.getAbsolutePath() + ".csv");
		}

		try {
			boolean created = f.createNewFile();
		} catch (IOException e) {
			showAlert(Alert.AlertType.ERROR, "Ошибка", e.toString(), e.getMessage());
		}

		WriteToFile.writeToFile(f, this.series, this.results);
		showAlert(Alert.AlertType.INFORMATION, "Успех", "Успешно сохранен результат", null);
	}
}
