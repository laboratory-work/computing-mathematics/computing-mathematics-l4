package ru.lanolin.compmath.l4.ui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import ru.lanolin.compmath.l4.math.XYSeries;
import ru.lanolin.compmath.l4.ui.FXMain;
import ru.lanolin.compmath.l4.utils.ReadFromFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class MainController {

	private FXMain application;
	public void setApplicationController(FXMain application){
		this.application = application;
	}

	@FXML public Button choiceFile;
	@FXML public TextField pathToFile;
	@FXML public TextArea dataPreview;
	@FXML public Button solveBTN;

	private XYSeries series;

	private FileChooser chooser;

	@FXML
	public void initialize(){
		chooser = new FileChooser();
		FileChooser.ExtensionFilter csv = new FileChooser.ExtensionFilter("CSV (*.csv)", "*.csv");
		chooser.getExtensionFilters().add(csv);
	}

	@FXML
	private void actionChoiceButton(){
		File data = chooser.showOpenDialog(application.getMainStage());
		if(data == null) return;
		pathToFile.setText(data.getAbsolutePath());
		try {
			series = ReadFromFile.read(data);
			String builder = "N = " + series.getN() + "\nX = " + Arrays.toString(series.getXi()) + "\nY = " + Arrays.toString(series.getYi());
			dataPreview.setText(builder);
		}catch (IOException | NumberFormatException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR, "Ошибка чтения", ButtonType.CLOSE);
			alert.setTitle("Ошибка чтения");
			alert.setHeaderText(e.toString());
			alert.setContentText(e.getMessage());
			alert.show();
			series = null;
			dataPreview.setText("");
		}
	}

	@FXML
	private void  actionSolveButton(){
		this.application.solveAction(series);
	}

}
