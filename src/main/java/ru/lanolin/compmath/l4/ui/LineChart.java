package ru.lanolin.compmath.l4.ui;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.chart.ui.ApplicationFrame;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RefineryUtilities;
import ru.lanolin.compmath.l4.math.Result;

import javax.swing.*;
import java.awt.*;

public class LineChart {

	private final JFreeChart chart;

	public LineChart(ru.lanolin.compmath.l4.math.XYSeries series, Result result) {
//		super(result.getFunction().getName());

		chart = ChartFactory.createXYLineChart(
				result.getFunction().getName(), "X", "Y", null,
				PlotOrientation.VERTICAL,
				true, false, false
		);

		chart.setBackgroundPaint(Color.white);

		final XYPlot plot = chart.getXYPlot();
		plot.setBackgroundPaint(new Color(159, 190, 237));

		plot.setDomainGridlinePaint(Color.gray);
		plot.setRangeGridlinePaint(Color.gray);
		plot.setAxisOffset(new RectangleInsets(1.0, 1.0, 1.0, 1.0));

		plot.getDomainAxis().setAxisLineVisible(true);

		final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setAxisLineVisible(true);
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

		XYSeries tableSeries = new XYSeries("Исходные");
		XYSeries functionSeries = new XYSeries(result.getFunction().getName());

		for (int i = 0; i < series.getN(); i++) {
			tableSeries.add(new XYDataItem(series.getXi(i), series.getYi(i)));
			functionSeries.add(new XYDataItem(series.getXi(i), result.getF(i)));
		}

		XYDataset tableDataset = new XYSeriesCollection(tableSeries);

		XYSplineRenderer r0 = new XYSplineRenderer();
		r0.setPrecision(8);
		r0.setSeriesShapesVisible(0, true);
		r0.setSeriesLinesVisible (0, false);
		r0.setSeriesPaint(0, Color.blue);

		plot.setDataset(0, tableDataset);
		plot.setRenderer(0, r0);

		XYDataset functionDataset = new XYSeriesCollection(functionSeries);
		XYSplineRenderer r1 = new XYSplineRenderer();
		r1.setPrecision(8);
		r1.setSeriesPaint(0, Color.red);
		r1.setSeriesShapesVisible(0, true);
		r1.setSeriesLinesVisible (0, false);

		plot.setDataset(1, functionDataset);
		plot.setRenderer(1, r1);
	}

	public ChartPanel plot() {
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setPreferredSize(new Dimension(500, 400));
		return chartPanel;
	}
}
