package ru.lanolin.compmath.l4.math;

import ru.lanolin.compmath.l4.utils.BigDecimalUtils;

import java.math.BigDecimal;
import java.math.MathContext;

public class PowerApproximation implements Approximation{

	@Override
	public Result solve(XYSeries series) {
		int n = series.getN();
		BigDecimal[] xi = new BigDecimal[n];
		BigDecimal[] yi = new BigDecimal[n];
		System.arraycopy(series.getXi(), 0, xi, 0, n);
		System.arraycopy(series.getYi(), 0, yi, 0, n);

		for (int i = 0; i < n; i++) {
			yi[i] = BigDecimalUtils.ln(yi[i], 32);
			xi[i] = BigDecimalUtils.ln(xi[i], 32);
		}
		XYSeries newSeries = new XYSeries(n, xi, yi);
		Result ans = Approximations.Linear.getMethod().solve(newSeries);

		ans.setA(BigDecimalUtils.exp(ans.getA(), 32));
		BigDecimal S = BigDecimal.ZERO;

		for (int i = 0; i < n; i++) {
			ans.setF(i, BigDecimalUtils.exp(ans.getF(i), 32));
			BigDecimal eps = delta(ans.getF(i), series.getYi(i));
			S = S.add(eps.pow(2));
		}

		BigDecimal sigma = S.divide(BigDecimal.valueOf(n), MathContext.DECIMAL128).sqrt(MathContext.DECIMAL128);
		ans.setSigma(sigma);
		ans.setS(S);
		return ans;
	}

}
