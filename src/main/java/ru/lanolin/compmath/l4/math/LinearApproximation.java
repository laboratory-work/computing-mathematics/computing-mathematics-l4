package ru.lanolin.compmath.l4.math;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Arrays;
import java.util.stream.IntStream;

public class LinearApproximation implements Approximation{

	@Override
	public Result solve(XYSeries series) {
		int seriesN = series.getN();

		BigDecimal n = BigDecimal.valueOf(seriesN);
		BigDecimal sx = sX(series, 1);
		BigDecimal sy = sY(series);
		BigDecimal sxx = sX(series, 2);
		BigDecimal sxy = sXY(series);

		BigDecimal a = solveA(n, sx, sy, sxx, sxy);
		BigDecimal b = solveB(n, sx, sy, sxx, sxy);
		BigDecimal S = BigDecimal.ZERO;
		Result result = new Result(seriesN);

		for (int i = 0; i < seriesN; i++) {
			BigDecimal func = function(series.getXi(i), a, b);
			BigDecimal epsilon = delta(func, series.getYi(i));

			S = S.add(epsilon.pow(2));
			result.setF(i, func);
		}

		BigDecimal sigma = S.divide(n, MathContext.DECIMAL128).sqrt(MathContext.DECIMAL128);

		result.setSigma(sigma);
		result.setA(a);
		result.setB(b);
		result.setS(S);

		return result;
	}

//	private BigDecimal cor(XYSeries series){
//		int n = series.getN();
//		BigDecimal nB = BigDecimal.valueOf(n);
//		BigDecimal[] xi = series.getXi();
//		BigDecimal[] yi = series.getYi();
//
//		BigDecimal avX = Arrays.stream(xi)
//				.reduce(BigDecimal.ZERO, BigDecimal::add)
//				.divide(nB, MathContext.DECIMAL128)
//				.negate();
//
//		BigDecimal avY = Arrays.stream(yi)
//				.reduce(BigDecimal.ZERO, BigDecimal::add)
//				.divide(nB, MathContext.DECIMAL128)
//				.negate();
//
//		BigDecimal numerator = IntStream.range(0, n)
//				.mapToObj(i -> xi[i].add(avX).multiply(yi[i].add(avY)))
//				.reduce(BigDecimal.ZERO, BigDecimal::add);
//
//		BigDecimal denominator1 = IntStream.range(0, n)
//				.mapToObj(i -> xi[i].add(avX).pow(2))
//				.reduce(BigDecimal.ZERO, BigDecimal::add);
//
//		BigDecimal denominator2 = IntStream.range(0, n)
//				.mapToObj(i -> yi[i].add(avY).pow(2))
//				.reduce(BigDecimal.ZERO, BigDecimal::add);
//
//		BigDecimal denominator = denominator1.multiply(denominator2).sqrt(MathContext.DECIMAL128);
//		return numerator.divide(denominator, MathContext.DECIMAL128);
//	}

	private BigDecimal function(BigDecimal x, BigDecimal a, BigDecimal b){
		return a.multiply(x).add(b);
	}

	private BigDecimal solveA(BigDecimal n, BigDecimal sx, BigDecimal sy, BigDecimal sxx, BigDecimal sxy){
		BigDecimal numerator = sxy.multiply(n).add(sx.multiply(sy).negate());
		BigDecimal denominator = sxx.multiply(n).add(sx.pow(2).negate());
		return numerator.divide(denominator, MathContext.DECIMAL128);
	}

	private BigDecimal solveB(BigDecimal n, BigDecimal sx, BigDecimal sy, BigDecimal sxx, BigDecimal sxy) {
		BigDecimal numerator = sxx.multiply(sy).add(sx.multiply(sxy).negate());
		BigDecimal denominator = sxx.multiply(n).add(sx.pow(2).negate());
		return numerator.divide(denominator, MathContext.DECIMAL128);
	}

}
