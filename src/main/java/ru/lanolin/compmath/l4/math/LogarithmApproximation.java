package ru.lanolin.compmath.l4.math;

import ru.lanolin.compmath.l4.utils.BigDecimalUtils;

import java.math.BigDecimal;
import java.math.MathContext;

public class LogarithmApproximation implements Approximation{

	@Override
	public Result solve(XYSeries series) {
		int n = series.getN();
		BigDecimal[] xi = new BigDecimal[n];
		BigDecimal[] yi = new BigDecimal[n];
		System.arraycopy(series.getXi(), 0, xi, 0, n);
		System.arraycopy(series.getYi(), 0, yi, 0, n);

		for (int i = 0; i < n; i++) {
			xi[i] = BigDecimalUtils.ln(xi[i], 32);
		}

		XYSeries newSeries = new XYSeries(n, xi, yi);

		return Approximations.Linear.getMethod().solve(newSeries);
	}
}
