package ru.lanolin.compmath.l4.math;

import java.math.BigDecimal;
import java.util.Arrays;

public class XYSeries {

	private int n;
	private BigDecimal[][] xyi;

	public XYSeries(int n){
		this.n = n;
		this.xyi = new BigDecimal[2][n];
	}

	public XYSeries(int n, BigDecimal[] xi, BigDecimal[] yi) {
		this.n = n;
		this.xyi = new BigDecimal[2][];
		this.xyi[0] = xi;
		this.xyi[1] = yi;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public BigDecimal[] getXi() {
		return xyi[0];
	}

	public void setXi(BigDecimal[] xi) {
		this.xyi[0] = xi;
	}

	public BigDecimal[] getYi() {
		return xyi[1];
	}

	public void setYi(BigDecimal[] yi) {
		this.xyi[1] = yi;
	}

	public void setXi(int i, BigDecimal xi) {
		this.xyi[0][i] = xi;
	}

	public void setYi(int i, BigDecimal yi) {
		this.xyi[1][i] = yi;
	}

	public BigDecimal getXi(int i){
		return xyi[0][i];
	}

	public BigDecimal getYi(int i){
		return xyi[1][i];
	}

	public void set(int i, BigDecimal x, BigDecimal y){
		this.xyi[0][i] = x;
		this.xyi[1][i] = y;
	}

	public BigDecimal[][] getXY() {
		return xyi;
	}

	@Override
	public String toString() {
		return "XYSeries{" + "\nn=" + n + ", \nxi=" + Arrays.toString(xyi[0]) + ", \nyi=" + Arrays.toString(xyi[1]) + "\n}";
	}
}
