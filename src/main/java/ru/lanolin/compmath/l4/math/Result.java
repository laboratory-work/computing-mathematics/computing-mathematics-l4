package ru.lanolin.compmath.l4.math;

import java.math.BigDecimal;
import java.util.Arrays;

public class Result {

	private Approximations function;
	private BigDecimal S;
	private BigDecimal a, b, c;
	private BigDecimal sigma;
	private BigDecimal[] f;

	public Result(int n) {
		f = new BigDecimal[n];
	}

	@Override
	public String toString() {
		return "Result{" +
				"\nS=" + S +
				", \na=" + a +
				", \nb=" + b +
				", \nc=" + c +
				", \nsigma=" + sigma +
				", \nf=" + Arrays.toString(f) +
				"\n}";
	}

	public BigDecimal[] getF() {
		return f;
	}

	public void setF(BigDecimal[] f) {
		this.f = f;
	}

	public BigDecimal getF(int i) {
		return f[i];
	}

	public void setF(int i, BigDecimal f) {
		this.f[i] = f;
	}

	public BigDecimal getS() {
		return S;
	}

	public void setS(BigDecimal s) {
		S = s;
	}

	public BigDecimal getA() {
		return a;
	}

	public void setA(BigDecimal a) {
		this.a = a;
	}

	public BigDecimal getB() {
		return b;
	}

	public void setB(BigDecimal b) {
		this.b = b;
	}

	public BigDecimal getC() {
		return c;
	}

	public void setC(BigDecimal c) {
		this.c = c;
	}

	public BigDecimal getSigma() {
		return sigma;
	}

	public void setSigma(BigDecimal sigma) {
		this.sigma = sigma;
	}

	public Approximations getFunction() {
		return function;
	}

	public void setFunction(Approximations function) {
		this.function = function;
	}
}
