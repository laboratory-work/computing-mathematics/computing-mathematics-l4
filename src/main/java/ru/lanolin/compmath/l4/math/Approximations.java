package ru.lanolin.compmath.l4.math;

public enum Approximations {
	Linear("Linear F=a*x+b", new LinearApproximation()),
	Exponential("Exponential F=a*e^(b*x)", new ExponentialApproximation()),
	Logarithm("Logarithm F=a*ln(x)+b", new LogarithmApproximation()),
	Polinom("Polinom F=a*x^2+b*x+c", new PolinomApproximation()),
	Power("Power F=a*x^b", new PowerApproximation())
	;
	private final Approximation method;
	private final String name;

	Approximations(String name, Approximation method) {
		this.method = method;
		this.name = name;
	}

	public Approximation getMethod() {
		return method;
	}

	public String getName() {
		return name;
	}
}
