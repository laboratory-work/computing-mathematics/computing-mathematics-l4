package ru.lanolin.compmath.l4.math;

import java.math.BigDecimal;
import java.math.MathContext;

import ru.lanolin.compmath.l4.utils.BigDecimalUtils;

public class ExponentialApproximation implements Approximation{

	@Override
	public Result solve(XYSeries series) {
		int n = series.getN();
		BigDecimal[] xi = new BigDecimal[n];
		BigDecimal[] yi = new BigDecimal[n];
		System.arraycopy(series.getXi(), 0, xi, 0, n);
		System.arraycopy(series.getYi(), 0, yi, 0, n);

		for (int i = 0; i < n; i++) {
			yi[i] = BigDecimalUtils.ln(yi[i], 32);
		}
		XYSeries newSeries = new XYSeries(n, xi, yi);
		Result lin = Approximations.Linear.getMethod().solve(newSeries);

		BigDecimal a = lin.getA();
		BigDecimal b = lin.getB();
		BigDecimal[] fx = lin.getF();
		BigDecimal S = BigDecimal.ZERO;

		Result ans = new Result(n);

		a = BigDecimalUtils.exp(a, 32);
		for (int i = 0; i < n; i++) {
			fx[i] = BigDecimalUtils.exp(fx[i], 32);
			BigDecimal eps = delta(fx[i], series.getYi(i));
			S = S.add(eps.pow(2));

			ans.setF(i, fx[i]);
		}

		BigDecimal sigma = S.divide(BigDecimal.valueOf(n), MathContext.DECIMAL128).sqrt(MathContext.DECIMAL128);

		ans.setSigma(sigma);
		ans.setA(a);
		ans.setB(b);
		ans.setS(S);

		return ans;
	}
}
