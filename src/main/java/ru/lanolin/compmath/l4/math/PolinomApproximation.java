package ru.lanolin.compmath.l4.math;

import java.math.BigDecimal;
import java.math.MathContext;

public class PolinomApproximation implements Approximation{

	@Override
	public Result solve(XYSeries series) {
		int n = series.getN();
		BigDecimal nB = BigDecimal.valueOf(n);

		BigDecimal sx1 = sX(series, 1);
		BigDecimal sx2 = sX(series, 2);
		BigDecimal sx3 = sX(series, 3);
		BigDecimal sx4 = sX(series, 4);

		BigDecimal sy = sY(series);
		BigDecimal sxy = sXY(series, 1);
		BigDecimal sx2y = sXY(series, 2);

		BigDecimal a = solveA(nB, sx1, sx2, sx3, sx4, sy, sxy, sx2y);
		BigDecimal b = solveB(nB, sx1, sx2, sx3, sx4, sy, sxy, sx2y);
		BigDecimal c = solveC(nB, sx1, sx2, sx3, sx4, sy, sxy, sx2y);

		Result result = new Result(series.getN());
		BigDecimal S = BigDecimal.ZERO;

		for (int i = 0; i < n; i++) {
			BigDecimal func = function(series.getXi(i), a, b, c);
			BigDecimal epsilon = delta(func, series.getYi(i));

			S = S.add(epsilon.pow(2));
			result.setF(i, func);
		}

		BigDecimal sigma = S.divide(nB, MathContext.DECIMAL128).sqrt(MathContext.DECIMAL128);

		result.setSigma(sigma);
		result.setA(a);
		result.setB(b);
		result.setC(c);
		result.setS(S);

		return result;
	}

	private BigDecimal solveA(BigDecimal n,
							  BigDecimal sx1, BigDecimal sx2, BigDecimal sx3, BigDecimal sx4,
							  BigDecimal sy, BigDecimal sxy, BigDecimal sx2y
	){

		//-sx1^2 sx2y + n sx2 sx2y                 + sx1 sx2 sxy - n sx3 sxy - sx2^2 sy + sx1 sx3 sy
		BigDecimal numerator =
				sx1.pow(2).multiply(sx2y).negate()
				.add(n.multiply(sx2).multiply(sx2y))
				.add(sx1.multiply(sx2).multiply(sxy))
				.add(n.multiply(sx3).multiply(sxy).negate())
				.add(sx2.pow(2).multiply(sy).negate())
				.add(sx1.multiply(sx3).multiply(sy));
		//sx2^3 - 2 sx1 sx2 sx3 + n sx3^2   + sx1^2 sx4 - n sx2 sx4
		return numerator.divide(getDenominator(n, sx1, sx2, sx3, sx4), MathContext.DECIMAL128).negate();
	}

	private BigDecimal solveB(BigDecimal n,
							  BigDecimal sx1, BigDecimal sx2, BigDecimal sx3, BigDecimal sx4,
							  BigDecimal sy, BigDecimal sxy, BigDecimal sx2y
	){

		//sx1 sx2 sx2y - n sx2y sx3 - sx2^2 sxy + n sx4 sxy + sx2 sx3 sy - sx1 sx4 sy
		BigDecimal numerator =
				sx1.multiply(sx2).multiply(sx2y)
				.add(n.multiply(sx2y).multiply(sx3).negate())
				.add(sx2.pow(2).multiply(sxy).negate())
				.add(n.multiply(sx4).multiply(sxy))
				.add(sx2.multiply(sx3).multiply(sy))
				.add(sx1.multiply(sx4).multiply(sy).negate());
		//sx2^3 - 2 sx1 sx2 sx3 + n sx3^2 + sx1^2 sx4 - n sx2 sx4
		return numerator.divide(getDenominator(n, sx1, sx2, sx3, sx4), MathContext.DECIMAL128).negate();
	}

	private BigDecimal solveC(BigDecimal n,
							  BigDecimal sx1, BigDecimal sx2, BigDecimal sx3, BigDecimal sx4,
							  BigDecimal sy, BigDecimal sxy, BigDecimal sx2y
	){
		//-sx2^2 sx2y + sx1 sx2y sx3 + sx2 sx3 sxy - sx1 sx4 sxy - sx3^2 sy + sx2 sx4 sy
		BigDecimal numerator =
				sx2.pow(2).multiply(sx2y).negate()
						.add(sx1.multiply(sx2y).multiply(sx3))
						.add(sx2.multiply(sx3).multiply(sxy))
						.add(sx1.multiply(sx4).multiply(sxy).negate())
						.add(sx3.pow(2).multiply(sy).negate())
						.add(sx2.multiply(sx4).multiply(sy));
		//sx2^3 - 2 sx1 sx2 sx3 + n sx3^2   + sx1^2 sx4 - n sx2 sx4
		return numerator.divide(getDenominator(n, sx1, sx2, sx3, sx4), MathContext.DECIMAL128).negate();
	}

	private BigDecimal getDenominator(BigDecimal n, BigDecimal sx1, BigDecimal sx2, BigDecimal sx3, BigDecimal sx4) {
		return sx2.pow(3)
				.add(BigDecimal.valueOf(2).multiply(sx1).multiply(sx2).multiply(sx3).negate())
				.add(n.multiply(sx3.pow(2)))
				.add(sx1.pow(2).multiply(sx4))
				.add(n.multiply(sx2).multiply(sx4).negate());
	}

	private BigDecimal function(BigDecimal x, BigDecimal a, BigDecimal b, BigDecimal c){
		return a.multiply(x.pow(2)).add(b.multiply(x)).add(c);
	}

}
