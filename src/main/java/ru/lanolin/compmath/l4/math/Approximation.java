package ru.lanolin.compmath.l4.math;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.stream.IntStream;

public interface Approximation {

	Result solve(XYSeries series);

	default BigDecimal delta(BigDecimal y, BigDecimal Y){
		return y.add(Y.negate());
	}

	default BigDecimal sX(XYSeries series, int pow){
		return Arrays.stream(series.getXi())
				.map(x -> x.pow(pow))
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	default BigDecimal sY(XYSeries series){
		return Arrays.stream(series.getYi()).reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	default BigDecimal sXY(XYSeries series){
		return sXY(series, 1);
	}

	default BigDecimal sXY(XYSeries series, int powX){
		return IntStream.range(0, series.getN())
				.mapToObj(i -> series.getXi(i).pow(powX).multiply(series.getYi(i)))
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}
}
